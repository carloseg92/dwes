<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title></title>
</head>
<body>
	<h1>Mi primera pagina</h1>
	<?php
	echo "<p>Hola mundo</p>";
	echo "<p>Estamos en nuestra primera clase de php</p>";
	$color = "rojo";
	echo "Mi coche es ". $color . "<br/>";
	echo "Mi coche es $color <br/>";

	echo 'Mi coche es '. $color . '<br/>';
	echo 'Mi coche es $color <br/>';

	var_dump($color);

	$frase = 'Arquimedes gritaba: "Eureka, Eureka" mientras celebraba su descubrimiento';
	echo "<br/>"; 
	echo $frase;
	echo "<br/>";
	echo "La variable color en mayusculas tiene valor " . strtoupper($color);
	echo "<br/>";
	echo "La variable color en minusculas tiene valor " . strtolower($color);
	echo "Esta es la prueba del jueves dia 30";
	?>
	<p>Tenemos clase lunes, miercoles y viernes</p>
</body>
</html>
